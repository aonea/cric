package cric.util.convertors;

import com.amazonaws.services.dynamodbv2.document.Item;

public interface ObjectConvertor {
    Item toItem(Object object);

    Object fromItem(Item item);
}

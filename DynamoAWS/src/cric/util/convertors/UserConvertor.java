package cric.util.convertors;

import com.amazonaws.services.dynamodbv2.document.Item;
import cric.model.User;

public class UserConvertor implements ObjectConvertor {
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String ID = "id";

    @Override
    public Item toItem(Object object) {
        User user = (User) object;
        return new Item()
                .withPrimaryKey(USERNAME, user.getUsername())
                .withString(PASSWORD, user.getPassword())
                .withString(ID, user.getId());
    }

    @Override
    public User fromItem(Item item) {
        User user = new User();
        user.setId(item.getString(ID));
        user.setUsername(item.getString(USERNAME));
        user.setPassword(item.getString(PASSWORD));
        return user;
    }
}

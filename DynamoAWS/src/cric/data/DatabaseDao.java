package cric.data;

public interface DatabaseDao {
    Object getItem(String tableName, String primaryColumn, String primaryKey);

    void addItem(String tableName, Object item);
}

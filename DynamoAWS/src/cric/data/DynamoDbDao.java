package cric.data;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;

public class DynamoDbDao implements DatabaseDao {

    private DatabaseConnection connection;

    public DynamoDbDao() {
        this.connection = new DatabaseConnection();
    }

    @Override
    public Item getItem(String tableName, String primaryColumn, String primaryKey) {
        Table table = connection.getTable(tableName);
        return table.getItem(primaryColumn, primaryKey);
    }

    @Override
    public void addItem(String tableName, Object item) {
        Table table = connection.getTable(tableName);
        table.putItem((Item) item);
    }
}

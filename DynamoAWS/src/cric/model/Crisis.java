package cric.model;

public class Crisis {

    public static final String ID = "id";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String RADIUS = "radius";
    public static final String DESCRIPTION = "description";

    private String id;
    private Double longitude;
    private Double latitude;
    private Double radius;
    private String description;

    public Crisis(String id, Double longitude, Double latitude, Double radius, String description) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
        this.radius = radius;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getRadius() {
        return radius;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

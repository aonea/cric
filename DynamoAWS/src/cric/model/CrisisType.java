package cric.model;

public enum CrisisType {
    FIRE("fire"),
    EARTHQUAKE("earthquake"),
    FLOOD("flood"),
    SHOOTING("shooting"),
    TERRORIST_ATTACK("terrorist attack");

    private String type;

    CrisisType(String type) {
        this.type = type;
    }

    public String getName() {
        return this.type;
    }

}

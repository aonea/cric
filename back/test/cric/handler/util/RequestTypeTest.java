package cric.handler.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class RequestTypeTest {

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PATCH = "PATCH";
    public static final String DELETE = "DELETE";

    @Test
    public void getRequestType_GET_USER_OK() {
        String path = "/users/user";
        assertEquals(RequestType.GET_USER, RequestType.getRequestType(GET, path));
    }

    @Test
    public void getRequestType_POST_USER_OK() {
        String path = "/users";
        assertEquals(RequestType.POST_USER, RequestType.getRequestType(POST, path));
    }

    @Test
    public void getRequestType_PATCH_USER_OK() {
        String path = "/users/user";
        assertEquals(RequestType.UPDATE_USER, RequestType.getRequestType(PATCH, path));
    }


    @Test
    public void getRequestType_POST_PLACE_OK() {
        String path = "/places/user";
        assertEquals(RequestType.POST_PLACE, RequestType.getRequestType(POST, path));
    }

    @Test
    public void getRequestType_GET_PLACE_OK() {
        String path = "/places/user";
        assertEquals(RequestType.GET_PLACE, RequestType.getRequestType(GET, path));
    }

    @Test
    public void getRequestType_GET_PLACE_BY_ID_OK() {
        String path = "/places/user/id234";
        assertEquals(RequestType.GET_PLACE_WITH_ID, RequestType.getRequestType(GET, path));
    }

    @Test
    public void getRequestType_UPDATE_PLACE_WITH_ID_OK() {
        String path = "/places/user/id123";
        assertEquals(RequestType.UPDATE_PLACE_WITH_ID, RequestType.getRequestType(PATCH, path));
    }

    @Test
    public void getRequestType_DELETE_PLACE_OK() {
        String path = "/places/user/id123";
        assertEquals(RequestType.DELETE_PLACE, RequestType.getRequestType(DELETE, path));
    }

}
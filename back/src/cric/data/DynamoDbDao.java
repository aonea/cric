package cric.data;

import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

import java.util.*;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.xspec.UpdateItemExpressionSpec;

public class DynamoDbDao implements DatabaseDao {

    private DatabaseConnection connection;

    public DynamoDbDao() {
        this.connection = new DatabaseConnection();
    }

    @Override
    public Item getItem(String tableName, String primaryColumn, String primaryKey) {
        Table table = connection.getTable(tableName);
        return table.getItem(primaryColumn, primaryKey);
    }

    @Override
    public void addItem(String tableName, Object item) {
        Table table = connection.getTable(tableName);
        table.putItem((Item) item);
    }

    @Override
    public void updateItem(String tableName, String primaryColumn, String primaryKey, Object item) {
        deleteItem(tableName, primaryColumn, primaryKey);
        addItem(tableName, item);
    }

    @Override
    public void deleteItem(String tableName, String primaryColumn, String primaryKey) {
        Table table = connection.getTable(tableName);
        System.out.println("DAO: " + primaryKey);
        System.out.println("TABLE: " + tableName);
        System.out.println("PRIMARY KEY: "+primaryColumn);
        table.deleteItem(primaryColumn, primaryKey);
    }
}

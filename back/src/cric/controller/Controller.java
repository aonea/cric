package cric.controller;

import com.sun.net.httpserver.HttpServer;
import cric.handler.HomeHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class Controller {
    private static final String HOST_NAME = "192.168.43.52"; //or "localhost"
    private static final int PORT = 6969;
    protected static HttpServer httpServer;


    Controller() {


        try {
            if (httpServer == null) {
                httpServer = HttpServer.create(new InetSocketAddress(HOST_NAME, PORT), 3);
                httpServer.start();
                createGlobalContexts();
            }
        } catch (IOException e) {
            System.out.println("Could not create server/serverSocket");
        }
    }

    private void createGlobalContexts() {
        httpServer.createContext("/", new HomeHandler()); //home or login
        httpServer.createContext("/favicon.ico", (exchange) -> {
            byte[] bytes = Files.readAllBytes(Paths.get("../front/res/imageedit_1_7587001916.png"));
            exchange.sendResponseHeaders(200, bytes.length);
            OutputStream responseOutputStream = exchange.getResponseBody();
            responseOutputStream.write(bytes);
            responseOutputStream.close();
        });
    }


    abstract void createLocalContexts();
}

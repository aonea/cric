package cric.controller;

import cric.handler.CrisisHandler;

import java.io.IOException;

public class CrisisController extends Controller {
    public CrisisController() {
        createLocalContexts();
    }

    @Override
    void createLocalContexts() {
        httpServer.createContext("/crises", new CrisisHandler());
    }
}

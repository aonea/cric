package cric.controller;

import cric.handler.PagesHandler;

import java.io.IOException;

public class PagesController extends Controller {
    public PagesController() {
        createLocalContexts();
    }

    @Override
    void createLocalContexts() {
        httpServer.createContext("/map_page", new PagesHandler());
        httpServer.createContext("/res", new PagesHandler());
        httpServer.createContext("/my_account_page", new PagesHandler());
        httpServer.createContext("/map_page", new PagesHandler());
        httpServer.createContext("/add_crisis_page", new PagesHandler());
        httpServer.createContext("/login_page", new PagesHandler());
    }
}

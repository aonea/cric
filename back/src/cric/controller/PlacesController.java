package cric.controller;

import cric.handler.PlacesHandler;

import java.io.IOException;

public class PlacesController extends Controller {
    public PlacesController() {
        createLocalContexts();
    }

    @Override
    void createLocalContexts() {
        httpServer.createContext("/places", new PlacesHandler());
    }
}

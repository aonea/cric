package cric.controller;

import cric.handler.NotificationHandler;
import cric.handler.LoginHandler;
import cric.handler.UsersHandler;

import java.io.IOException;

public class UsersController extends Controller {
    public UsersController() {
        createLocalContexts();
    }

    @Override
    void createLocalContexts() {
        httpServer.createContext("/users/login", new LoginHandler());
        httpServer.createContext("/users", new UsersHandler());
        httpServer.createContext("/notifications", new NotificationHandler());
    }
}

package cric.util.convertors;

import com.amazonaws.services.dynamodbv2.document.Item;
import cric.model.Crisis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CrisisConverter implements ObjectConverter {

    private static final String ID = "id";
    private static final String LONGITUDE = "longitude";
    private static final String LATITUDE = "latitude";
    private static final String RADIUS = "radius";
    private static final String DESCRIPTION = "description";
    private static final String TYPE = "type";
    private static final String STATUS = "status";
    private static final String BEGIN_DATE = "begin_date";
    private static final String END_DATE = "end_date";

    @Override
    public Item toItem(Object object) {
        Crisis crisis = (Crisis) object;
        return new Item()
                .withPrimaryKey(ID, crisis.getId())
                .withNumber(LONGITUDE, crisis.getLongitude())
                .withNumber(LATITUDE, crisis.getLatitude())
                .withNumber(RADIUS, crisis.getRadius())
                .withString(DESCRIPTION, crisis.getDescription())
                .withString(TYPE, crisis.getType())
                .withBoolean(STATUS, crisis.getStatus())
                .withString(BEGIN_DATE, crisis.getBeginDate().toString())
                .withString(END_DATE, crisis.getBeginDate().toString());
    }

    @Override
    public Crisis fromItem(Item item) {
//        DateFormat format = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy", Locale.ENGLISH);
//        Date beginDate = null;
//        Date endDate = null;
//        try {
//            beginDate = format.parse(item.getString(BEGIN_DATE));
//            endDate = format.parse(item.getString(END_DATE));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        Crisis crisis = new Crisis.Builder(item.getString(ID), item.getDouble(LONGITUDE), item.getDouble(LATITUDE), item.getDouble(RADIUS))
                .description(item.getString(DESCRIPTION))
                .type(item.getString(TYPE))
                .status(item.getBOOL(STATUS))
                .beginDate(item.getString(BEGIN_DATE))
                .endDate(item.getString(END_DATE))
                .build();
//        if(beginDate!=null){
//            crisis.setBeginDate(beginDate);
//        }
//        if(endDate!=null){
//            crisis.setEndDate(endDate);
//        }
        return crisis;
    }
}
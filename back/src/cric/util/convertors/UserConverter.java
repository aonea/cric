package cric.util.convertors;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.google.gson.Gson;
import cric.model.Subscription;
import cric.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserConverter implements ObjectConverter {
    private static final String ID = "id";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String ROLE = "role";
    private static final String PLACES = "places";
    private static final String SUBSCRIPTION = "subscription";
    private static final String SALT = "salt";
    private static final String LOGIN_DATE = "loginDate";

    @Override
    public Item toItem(Object object) {
        User user = (User) object;
        List<String> subscriptionList = new ArrayList<>();
        for (Subscription subscription : user.getSubscription()) {
            subscriptionList.add(new Gson().toJson(subscription));
        }
        return new Item()
                .withPrimaryKey(USERNAME, user.getUsername())
                .withString(PASSWORD, user.getPassword())
                .withString(ROLE, user.getRole())
                .withList(PLACES, user.getPlaces())
                .withString(SALT, user.getSalt())
                .withString(LOGIN_DATE, user.getLoginDate())
                .withList(SUBSCRIPTION, subscriptionList);
    }

    @Override
    public User fromItem(Item item) {
        List<String> stringList;
        List<Subscription> subscriptionList = new ArrayList<>();
        stringList = item.getList(SUBSCRIPTION);
        for (String string : stringList
                ) {
            subscriptionList.add(new Gson().fromJson(string, Subscription.class));
        }
        return new User.Builder(item.getString(USERNAME), item.getString(PASSWORD))
                .subscription(subscriptionList)
                .places(item.getList(PLACES))
                .role(item.getString(ROLE))
                .salt(item.getString(SALT))
                .loginDate(item.getString(LOGIN_DATE))
                .build();
    }
}

package cric.model;

import java.util.Map;
import java.util.TreeMap;

public class Subscription {
    private String endpoint;
    private String expirationTime;
    private Map<String, String> keys;

    public Subscription() {
        this.keys = new TreeMap<>();
    }

    public Subscription(String endpoint, String expirationTime, Map<String,String> keys) {
        this.endpoint = endpoint;
        this.expirationTime = expirationTime;
        this.keys = keys;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public void setExpirationTime(String expirationTime) {
        this.expirationTime = expirationTime;
    }

    public void setKeys(Map<String, String> keys) {
        this.keys = keys;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getExpirationTime() {
        return expirationTime;
    }

    public Map<String, String> getKeys() {
        return keys;
    }
}


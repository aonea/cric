package cric.model;

import java.util.Date;

public class Crisis {

    private String id;
    private Double longitude;
    private Double latitude;
    private Double radius;
    private String description;
    private String type;
    private Boolean status;
    private String beginDate;
    private String endDate;

    public static class Builder {
        private String id;
        private Double longitude;
        private Double latitude;
        private Double radius;
        private String description = "No description";
        private String type = "Unknown";
        private Boolean status = true;
        private String beginDate;
        private String endDate;

        public Builder(String id, Double longitude, Double latitude, Double radius) {
            this.id = id;
            this.longitude = longitude;
            this.latitude = latitude;
            this.radius = radius;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder status(Boolean status) {
            this.status = status;
            return this;
        }

        public Builder beginDate(String date) {
            this.beginDate = date;
            return this;
        }

        public Builder endDate(String date) {
            this.endDate = date;
            return this;
        }

        public Crisis build() {
            return new Crisis(this);
        }
    }

    private Crisis(Builder builder) {
        this.id = builder.id;
        this.longitude = builder.longitude;
        this.latitude = builder.latitude;
        this.radius = builder.radius;
        this.description = builder.description;
        this.type = builder.type;
        this.status = builder.status;
        this.beginDate = builder.beginDate;
        this.endDate = builder.endDate;
    }

    public String getId() {
        return id;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getRadius() {
        return radius;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getType() {

        return type;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

}

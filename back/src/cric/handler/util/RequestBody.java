package cric.handler.util;

import com.sun.net.httpserver.HttpExchange;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class RequestBody {
    public static Map<String, String> parseRequest(HttpExchange request) {
        Map<String, String> requestParameters = new HashMap<>();
        StringBuilder requestBody = new StringBuilder();

        try {
            InputStreamReader inputStreamReader = new InputStreamReader(request.getRequestBody(), "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            int b;
            requestBody = new StringBuilder(512);
            while ((b = bufferedReader.read()) != -1) {
                requestBody.append((char) b);
            }

            bufferedReader.close();
            inputStreamReader.close();
        } catch(Exception ex) {

        }

        return splitQuery(requestBody.toString());
    }

    private static Map<String, String> splitQuery(String query) {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String[] pairs = query.split("&");
        try {
            for (String pair : pairs) {
                int idx = pair.indexOf("=");
                query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            }
        } catch(Exception ex) {
        }

        return query_pairs;
    }
}

package cric.handler.util;

public class SessionCookie{
    private String username;
    private String sessionId;
    private String role;

    public void setUsername(String username) {
        this.username = username;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getRole() {
        return role;
    }
}
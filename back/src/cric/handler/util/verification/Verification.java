package cric.handler.util.verification;

import com.amazonaws.services.appstream.model.Session;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import cric.data.DynamoDbDao;
import cric.handler.util.SessionCookie;
import cric.model.User;
import cric.util.convertors.UserConverter;

import java.nio.charset.StandardCharsets;

public class Verification {
    private static DynamoDbDao dao = new DynamoDbDao();
    public static boolean verifyIdentity(SessionCookie cookie){
        if(cookie == null){
            return true;
        }
        User user = new UserConverter().fromItem(dao.getItem("users","username",cookie.getUsername()));
        String realSessionId = Hashing.sha256()
                .hashString(user.getUsername() + user.getPassword() + user.getLoginDate(), StandardCharsets.UTF_8)
                .toString();
        if(cookie.getSessionId().equals(realSessionId) && cookie.getRole().equals(user.getRole())){
            return true;
        } else {
            return false;
        }
    }

    public static boolean userRequiredVerification(SessionCookie cookie, String urlUser){
        if(cookie==null){
            return false;
        }
        if(cookie.getRole().equals("common user") && cookie.getUsername().equals(urlUser)){
            return true;
        } else {
            return false;
        }
    }

    public static boolean adminRequiredVerification(SessionCookie cookie){
        if(cookie == null){
            return false;
        }
        if(cookie.getRole().equals("admin")){
            return true;
        } else {
            return false;
        }
    }

    public static SessionCookie getCookie(Headers headers){
        if(headers.getFirst("Set-Cookie") == null){
            return null;
        } else{
            return new Gson().fromJson(headers.getFirst("Set-Cookie").split("=")[1], SessionCookie.class);
        }
    }
}

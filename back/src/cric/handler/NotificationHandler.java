package cric.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class NotificationHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) {
        System.out.println("someone unsubscribed");
        String response = "bye bye";
        OutputStream responseBodyStream = exchange.getResponseBody();
        try {
            exchange.sendResponseHeaders(200, response.getBytes().length);
            responseBodyStream.write(response.getBytes());
        } catch (IOException e) {
            System.out.println("Could not send response to client");
        } finally {
            try {
                responseBodyStream.close();
            } catch (IOException e) {
                System.out.println("Could not close output stream");
            }
        }
    }
}

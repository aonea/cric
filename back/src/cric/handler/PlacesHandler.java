package cric.handler;

import com.amazonaws.util.IOUtils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import cric.handler.repositories.PlacesRepo;
import cric.handler.util.RequestType;
import cric.handler.util.Response;
import cric.handler.util.verification.Verification;

import java.io.IOException;
import java.io.OutputStream;

public class PlacesHandler  implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) {
        Response response = new Response();
        PlacesRepo repo = new PlacesRepo();
        String username = exchange.getRequestURI().getPath().split("/")[2];
        if (!Verification.verifyIdentity(Verification.getCookie(exchange.getRequestHeaders())) ||
                !Verification.userRequiredVerification(Verification.getCookie(exchange.getRequestHeaders()),username)) {
            response = new Response("forbidden", 403);
        } else {
            switch (RequestType.getRequestType(exchange.getRequestMethod(), exchange.getRequestURI().getPath())) {
                case GET_PLACE: {
                    String userID = exchange.getRequestURI().getPath().split("/")[2];
                    response = repo.getPlace(userID);
                    break;
                }
                case GET_PLACE_WITH_ID: {
                    String userID = exchange.getRequestURI().getPath().split("/")[2];
                    String placeID = exchange.getRequestURI().getPath().split("/")[3];
                    response = repo.getPlaceById(userID, placeID);
                    break;
                }
                case POST_PLACE: {
                    String placeJson = null;
                    try {
                        placeJson = IOUtils.toString(exchange.getRequestBody());
                        String userID = exchange.getRequestURI().getPath().split("/")[2];
                        response = repo.postPlace(placeJson, userID);
                    } catch (IOException e) {
                        response = new Response("",500);
                    }
                    break;
                }
                case UPDATE_PLACE_WITH_ID: {
                    String placeJson = null;
                    try {
                        placeJson = IOUtils.toString(exchange.getRequestBody());
                        String userID = exchange.getRequestURI().getPath().split("/")[2];
                        String placeID = exchange.getRequestURI().getPath().split("/")[3];
                        response = repo.updatePlace(placeJson, userID, placeID);
                    } catch (IOException e) {
                        response = new Response("",500);
                    }
                    break;
                }
                case DELETE_PLACE: {
                    String placeID = exchange.getRequestURI().getPath().split("/")[3];
                    response = repo.deletePlace(username, placeID);
                    break;
                }
                default: {
                    break;
                }
            }
        }

        sendResponse(exchange,response);
    }

    private void sendResponse(HttpExchange exchange, Response response){
        System.out.println("someone reached places " + exchange.getRequestURI().getPath());
        exchange.getResponseHeaders().set("Content-type", "application/json");
        OutputStream responseBodyStream = exchange.getResponseBody();
        try {
            exchange.sendResponseHeaders(response.getCode(), response.getBody().getBytes().length);
            responseBodyStream.write(response.getBody().getBytes());
        } catch (IOException e) {
            System.out.println("!!! Could not write to client");;
        } finally {
            try {
                responseBodyStream.close();
            } catch (IOException e) {
                System.out.println("!!! Could not close output stream");
            }

        }
    }
}

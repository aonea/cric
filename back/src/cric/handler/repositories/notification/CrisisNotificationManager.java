package cric.handler.repositories.notification;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.google.gson.Gson;
import cric.data.DatabaseConnection;
import cric.data.DynamoDbDao;
import cric.data.util.Tables;
import cric.model.Crisis;
import cric.model.Place;
import cric.model.Subscription;
import cric.model.User;
import cric.subs.SendNotification;
import cric.util.convertors.PlacesConverter;
import cric.util.convertors.UserConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

public class CrisisNotificationManager implements Runnable {

    private String payload;
    private Crisis crisis;

    public CrisisNotificationManager(Crisis crisis, String payload) {
        this.crisis = crisis;
        this.payload = payload;
    }

    @Override
    public void run() {
        Set<String> users = new HashSet<>();

        //scan the places table
        ScanRequest scanRequest = new ScanRequest().withTableName(Tables.PLACES.getName());
        ScanResult result = DatabaseConnection.getClient().scan(scanRequest);

        //for each line in places
        for (Map<String, AttributeValue> map : result.getItems()) {
            try {
                AttributeValue v = map.get(Tables.PLACES.primaryKey());
                //checks if place is in danger
                String user = checkPlace(v.getS(), crisis);
                if (user != null) {
                    //if so, the user is added to the notification list
                    users.add(user);
                }
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        }

        //foreach user, send notification to all registered endpoints
        for (String user : users) {
            sendNotification(user, payload);
        }
    }

    private String checkPlace(String id, Crisis crisis) {
        DynamoDbDao dao = new DynamoDbDao();
        Place place = new PlacesConverter().fromItem(dao.getItem(Tables.PLACES.getName(), Tables.CRISES.primaryKey(), id));
        if (distanceBetweenCoordinates(place.getLatitude(), place.getLongitude(), crisis.getLatitude(), crisis.getLongitude())
                <= crisis.getRadius()) {
            return place.getUserId();
        }
        return null;
    }

    public Double distanceBetweenCoordinates(Double lat1, Double long1, Double lat2, Double long2) {
        final Double earthRadius = 6371.0;
        Double dLat = Math.toRadians(lat2 - lat1);
        Double dLong = Math.toRadians(long2 - long1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLong / 2) * Math.sin(dLong / 2) * Math.cos(lat1) * Math.cos(lat2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadius * c * 1000;
    }

    private void sendNotification(String username, String payload) {

        DynamoDbDao dao = new DynamoDbDao();
        User user = new UserConverter().fromItem(dao.getItem(Tables.USERS.getName(),Tables.USERS.primaryKey(),username));
        for (Subscription subs: user.getSubscription()
             ) {
            try {
                Security.addProvider(new BouncyCastleProvider());
                SendNotification sender = new SendNotification();

                sender.run(new Gson().toJson(subs),payload);


            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

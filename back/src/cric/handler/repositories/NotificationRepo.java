package cric.handler.repositories;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.google.gson.Gson;
import cric.data.DynamoDbDao;
import cric.data.util.Tables;
import cric.handler.util.Response;
import cric.model.User;
import cric.util.convertors.UserConverter;

import java.util.ArrayList;

public class NotificationRepo {
    private DynamoDbDao dao;
    private UserConverter converter;
    private Gson gson;

    public NotificationRepo() {
        this.dao = new DynamoDbDao();
        this.converter = new UserConverter();
        this.gson = new Gson();
    }

    public Response setSubscription(String username, String auth){
        System.out.println("USERNAME: "+ username);
        Response response;
        Item item = dao.getItem(Tables.USERS.getName(), Tables.USERS.primaryKey(), username);
        if (item != null) {
            User user = converter.fromItem(item);
            //user.setSubscription(auth);
            item = converter.toItem(user);
            dao.updateItem(Tables.USERS.getName(), Tables.USERS.primaryKey(), username, item);
            response = new Response(gson.toJson(user),200);
        } else {
            response = new Response("{\"message\" : \"user was not found\"}", 404);
        }
        return response;
    }
}

package cric.handler.repositories.notif;

import java.io.IOException;
import java.security.*;
import java.util.HashMap;
import java.util.Map;

public class PushService {}
  /* private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    //google cloud messaging api key;
    private String gcmApiKey;
    //jwt payload
    private String subject;
    //
    private PublicKey publicKey;

    private PrivateKey privateKey;

    public PushService(){}
    public PushService(String gcmApiKey){
        this.gcmApiKey = gcmApiKey;
    }
    public PushService(KeyPair keyPair, String subject){
        this.publicKey = keyPair.getPublic();
        this.privateKey = keyPair.getPrivate();
        this.subject = subject;
    }

    public PushService(String publicKey, String privateKey, String subject) throws GeneralSecurityException {
        this.publicKey = Utils.loadPublicKey(publicKey);
        this.privateKey = Utils.loadPrivateKey(privateKey);
        this.subject = subject;
    }

    public static Encrypted encrypt(byte[] buffer, PublicKey userPublicKey, byte[] userAuth, int padSize) throws GeneralSecurityException, IOException {
        ECNamedCurveParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec("prime256v1");

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("ECDH", "BC");
        keyPairGenerator.initialize(parameterSpec);

        KeyPair serverKey = keyPairGenerator.generateKeyPair();

        Map<String, KeyPair> keys = new HashMap<>();
        keys.put("server-key-id", serverKey);

        Map<String, String> labels = new HashMap<>();
        labels.put("server-key-id", "P-256");

        byte[] salt = new byte[16];
        SECURE_RANDOM.nextBytes(salt);

        HttpEce httpEce = new HttpEce(keys, labels);
        byte[] ciphertext = httpEce.encrypt(buffer, salt, null, "server-key-id", userPublicKey, userAuth, padSize);

        return new Encrypted.Builder()
                .withSalt(salt)
                .withPublicKey(serverKey.getPublic())
                .withCiphertext(ciphertext)
                .build();
    }



    public PushService setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;

        return this;
    }

    public PushService setPrivateKey(String privateKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
        setPrivateKey(Utils.loadPrivateKey(privateKey));

        return this;
    }

    public HttpResponse send(Notification notification) throws GeneralSecurityException, IOException, InterruptedException {
        return notification;
    }

    public PushService setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;

        return this;
    }

    protected boolean vapidEnabled() {
        return publicKey != null && privateKey != null;
    }

}*/

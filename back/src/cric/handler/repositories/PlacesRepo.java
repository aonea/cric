package cric.handler.repositories;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.google.gson.Gson;
import cric.data.DynamoDbDao;
import cric.data.util.Tables;
import cric.handler.util.Response;
import cric.model.Place;
import cric.model.User;
import cric.util.convertors.PlacesConverter;
import cric.util.convertors.UserConverter;
import javafx.scene.control.Tab;

import java.util.ArrayList;
import java.util.List;

public class PlacesRepo {

    private DynamoDbDao dao;
    private PlacesConverter converter;
    private UserConverter userConverter;
    private Gson gson;

    public PlacesRepo() {
        this.dao = new DynamoDbDao();
        this.converter = new PlacesConverter();
        this.gson = new Gson();
        this.userConverter = new UserConverter();
    }

    class UpdatePlace {
        private Double longitude;
        private Double latitude;
        private String name;
        private Boolean danger;

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getDanger() {
            return danger;
        }

        public void setDanger(Boolean danger) {
            this.danger = danger;
        }
    }

    public Response getPlace(String username) {

        List<String> places;
        List<Place> placesItem = new ArrayList<>();

        User user = userConverter.fromItem(dao.getItem(Tables.USERS.getName(), Tables.USERS.primaryKey(), username));
        places = user.getPlaces();
        for (String place : places) {
            placesItem.add(converter.fromItem(dao.getItem(Tables.PLACES.getName(), Tables.PLACES.primaryKey(), place)));
        }
        return new Response(gson.toJson(placesItem), 200);
    }

    public Response getPlaceById(String username, String placeID) {
        Response response = new Response();

        Item item = dao.getItem(Tables.PLACES.getName(), Tables.PLACES.primaryKey(), placeID);
        if (item != null) {
            Place place = converter.fromItem(item);
            response.setCode(200);
            response.setBody(gson.toJson(place));
        } else
            response = new Response("{\"message\" : \"place was not found\"}", 404);
        return response;
    }

    public Response postPlace(String placeJson, String username) {
        Response response;
        Place place = gson.fromJson(placeJson, Place.class);
        if (dao.getItem(Tables.PLACES.getName(), Tables.PLACES.primaryKey(), place.getId()) == null) {
            dao.addItem(Tables.PLACES.getName(), converter.toItem(place));
            User user = userConverter.fromItem(dao.getItem(Tables.USERS.getName(),Tables.USERS.primaryKey(),place.getUserId()));
            user.addPlace(place.getId());
            dao.addItem(Tables.USERS.getName(),new UserConverter().toItem(user));
            response = new Response("", 201);
        } else
            response = new Response("", 409);
        return response;
    }

    public Response updatePlace(String placeJson, String username, String placeID) {
        Response response;
        System.out.println("update place: "+placeJson);
        Place place = converter.fromItem(dao.getItem(Tables.PLACES.getName(), Tables.PLACES.primaryKey(), placeID));
        UpdatePlace newPlace = gson.fromJson(placeJson, UpdatePlace.class);
        if (newPlace.getDanger() != null){
            place.setDanger(newPlace.getDanger());
        }
        if (newPlace.getLatitude() != null){
            place.setLatitude(newPlace.getLatitude());
        }
        if (newPlace.getLongitude() != null){
            place.setLongitude(newPlace.getLongitude());
        }
        if (newPlace.getName() != null){
            place.setName(newPlace.getName());
        }
        dao.addItem(Tables.PLACES.getName(), converter.toItem(place));
        response = new Response("", 200);
        return response;
    }

    public Response deletePlace(String username, String placeID) {
        Response response = new Response("", 200);
        System.out.println("REPO: "+placeID);
        dao.deleteItem(Tables.PLACES.getName(), Tables.PLACES.primaryKey(), placeID);
        User user = userConverter.fromItem(dao.getItem(Tables.USERS.getName(),Tables.USERS.primaryKey(), username));
        user.getPlaces().remove(placeID);
        dao.addItem(Tables.USERS.getName(), userConverter.toItem(user));
        return response;
    }

}

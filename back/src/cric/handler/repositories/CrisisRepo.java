package cric.handler.repositories;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.google.gson.Gson;
import cric.data.DatabaseConnection;
import cric.data.DynamoDbDao;
import cric.data.util.Tables;
import cric.handler.repositories.notification.CrisisNotificationManager;
import cric.handler.util.Response;
import cric.model.Crisis;
import cric.util.convertors.CrisisConverter;

import java.util.*;

public class CrisisRepo {

    private DynamoDbDao dao;
    private CrisisConverter converter;
    private Gson gson;

    class UpdateCrisis {
        private Double longitude;
        private Double latitude;
        private Double radius;
        private String description;
        private String type;
        private Boolean status;
        private String endDate;

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getRadius() {
            return radius;
        }

        public void setRadius(Double radius) {
            this.radius = radius;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }
    }

    public CrisisRepo() {
        this.dao = new DynamoDbDao();
        this.converter = new CrisisConverter();
        this.gson = new Gson();
    }

    public Response getCrisis() {
        ArrayList<String> ids = new ArrayList<>();
        List<Crisis> crisisList = new ArrayList<>();
        ScanRequest scanRequest = new ScanRequest().withTableName(Tables.CRISES.getName());
        ScanResult result = DatabaseConnection.getClient().scan(scanRequest);

        for (Map<String, AttributeValue> map : result.getItems()) {
            try {
                AttributeValue v = map.get(Tables.CRISES.primaryKey());
                String id = v.getS();
                ids.add(id);
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        }
        for (String id : ids) {
            crisisList.add(converter.fromItem(dao.getItem(Tables.CRISES.getName(), Tables.CRISES.primaryKey(), id)));
        }
        return new Response(gson.toJson(crisisList), 200);
    }

    public Response postCrisis(String crisisJson) {
        Response response;
        Crisis newCrisis = gson.fromJson(crisisJson, Crisis.class);
        Item item = dao.getItem(Tables.CRISES.getName(), Tables.CRISES.primaryKey(), newCrisis.getId());
        if (item == null) {
            dao.addItem(Tables.CRISES.getName(), converter.toItem(newCrisis));
            response = new Response("", 201);
        } else {
            response = new Response("", 451);
        }
        new Thread(new CrisisNotificationManager(newCrisis, "Warning: place in danger")).start();
        return response;
    }

    public Response updateCrisis(String crisisJson, String idCrisis) {
        Response response;
        Crisis crisis = converter.fromItem(new DynamoDbDao().getItem(Tables.CRISES.getName(), Tables.CRISES.primaryKey(), idCrisis));
        Crisis newCrisis = gson.fromJson(crisisJson, Crisis.class);
        if (newCrisis.getLongitude() != null){
            crisis.setLongitude(newCrisis.getLongitude());
        }
        if (newCrisis.getLatitude() != null) {
            crisis.setLatitude(newCrisis.getLatitude());
        }
        if (newCrisis.getRadius() != null) {
            crisis.setRadius(newCrisis.getRadius());
        }
        if (newCrisis.getDescription() != null) {
            crisis.setDescription(newCrisis.getDescription());
        }
        if (newCrisis.getType() != null) {
            crisis.setType(newCrisis.getType());
        }

        if (newCrisis.getStatus() != null) {
            if (!newCrisis.getStatus()&& crisis.getStatus()) {
                new Thread(new CrisisNotificationManager(crisis, "Your place is marked as SAFE")).start();
            }
            crisis.setStatus(newCrisis.getStatus());
        }
        if (newCrisis.getEndDate() != null) {
            crisis.setEndDate(newCrisis.getEndDate());
        }
        dao.addItem(Tables.CRISES.getName(), converter.toItem(crisis));
        response = new Response("", 200);
        return response;
    }
}

package cric.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class HomeHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) {
        OutputStream responseBodyStream = exchange.getResponseBody();
        try {
            System.out.println("someone reached home");
            System.out.println(exchange.getRequestURI());
            exchange.getResponseHeaders().set("Location", "/map_page/map_page.html");
            exchange.sendResponseHeaders(302, 0);
        } catch (IOException e) {
            System.out.println("!!! Could not send response to client");
        } finally {
            try {
                responseBodyStream.close();
            } catch (IOException e) {
                System.out.println("!!! Could not close output stream");
            }
        }
    }
}

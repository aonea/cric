package cric.handler;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.util.IOUtils;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import cric.data.DynamoDbDao;
import cric.data.util.Tables;
import cric.handler.util.Response;
import cric.handler.util.SessionCookie;
import cric.model.User;
import cric.util.convertors.UserConverter;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class LoginHandler implements HttpHandler {

    public static final String SET_COOKIE = "Set-Cookie";

    private class UserDetails {
        private String username;
        private String password;
    }

    private Gson gson = new Gson();
    private UserConverter converter = new UserConverter();
    private DynamoDbDao dao = new DynamoDbDao();

    @Override
    public void handle(HttpExchange exchange) {
        Response response;
        try {

            UserDetails details = gson.fromJson(IOUtils.toString(exchange.getRequestBody()), UserDetails.class);
            Item item = dao.getItem(Tables.USERS.getName(), Tables.USERS.primaryKey(), details.username);
            if (item == null) {
                response = new Response("", 400);
            } else {
                User user = converter.fromItem(item);
                String hashedPassword = Hashing.sha256()
                        .hashString(details.password + user.getSalt(), StandardCharsets.UTF_8)
                        .toString();
                if (hashedPassword.equals(user.getPassword())) {
                    response = new Response("", 200);
                    user.setLoginDate(Long.toString(System.currentTimeMillis()));
                    dao.addItem(Tables.USERS.getName(), converter.toItem(user));
                    SessionCookie cookie = new SessionCookie();
                    cookie.setRole(user.getRole());
                    cookie.setUsername(user.getUsername());
                    cookie.setSessionId(Hashing.sha256()
                            .hashString(user.getUsername() + user.getPassword() + user.getLoginDate(), StandardCharsets.UTF_8)
                            .toString());
                    response.setHeaders(SET_COOKIE, "Session=" + gson.toJson(cookie) + "; Path=/;");
                } else {
                    response = new Response("", 400);
                }
            }
        } catch (IOException e) {
            response = new Response("", 500);
        }

        sendResponse(exchange, response);

    }

    private void sendResponse(HttpExchange exchange, Response response) {
        OutputStream responseBodyStream = exchange.getResponseBody();
        try {
            exchange.getResponseHeaders().add("Content-Type", "application/json");
            if (response.getHeaders().size() != 0) {
                exchange.getResponseHeaders().add(SET_COOKIE, response.getHeaders().get(SET_COOKIE));
            }
            exchange.sendResponseHeaders(response.getCode(), response.getBody().getBytes().length);
            responseBodyStream.write(response.getBody().getBytes());
        } catch (IOException e) {
            System.out.println("Could not send response to client");
        } finally {
            try {
                responseBodyStream.close();
            } catch (IOException e) {
                System.out.println("Could not close output stream");
            }
        }
    }
}

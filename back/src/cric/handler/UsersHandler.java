package cric.handler;

import com.amazonaws.util.IOUtils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import cric.handler.repositories.UsersRepo;
import cric.handler.util.RequestType;
import cric.handler.util.Response;
import cric.handler.util.verification.Verification;

import java.io.IOException;
import java.io.OutputStream;

public class UsersHandler implements HttpHandler {

    private static final String SET_COOKIE = "Set-Cookie";
    private UsersRepo usersRepo = new UsersRepo();

    @Override
    public void handle(HttpExchange exchange) {
        Response response = new Response();
        if (!Verification.verifyIdentity(Verification.getCookie(exchange.getRequestHeaders()))) {
            response = new Response("forbidden", 403);
        } else {
            switch (RequestType.getRequestType(exchange.getRequestMethod(), exchange.getRequestURI().getPath())) {
                case GET_USER: {
                    String username = exchange.getRequestURI().getPath().split("/")[2];
                    if (!Verification.userRequiredVerification(Verification.getCookie(exchange.getRequestHeaders()), username)) {
                        response = new Response("forbidden", 403);
                    } else {
                        response = usersRepo.getUser(username);
                    }
                    break;
                }
                case POST_USER: {
                    String json = null;
                    try {
                        json = IOUtils.toString(exchange.getRequestBody());
                        response = usersRepo.postUser(json);
                    } catch (IOException e) {
                        response = new Response("", 500);
                    }
                    break;
                }
                case UPDATE_USER: {
                    try {
                        String json = IOUtils.toString(exchange.getRequestBody());
                        String username = exchange.getRequestURI().getPath().split("/")[2];
                        if (!Verification.userRequiredVerification(Verification.getCookie(exchange.getRequestHeaders()), username)) {
                            response = new Response("", 403);
                        } else {
                            response = usersRepo.updateUser(json, username);
                        }
                    } catch (IOException e) {
                        response = new Response("", 500);
                    }
                    break;
                }
                default: {
                    response.setCode(400);
                    break;
                }
            }
        }

        sendResponse(exchange, response);
    }

    private void sendResponse(HttpExchange exchange, Response response) {
        System.out.println("someone reached users " + exchange.getRequestURI().getPath());
        if (response.getHeaders().size() != 0) {
            exchange.getResponseHeaders().add(SET_COOKIE, response.getHeaders().get(SET_COOKIE));
        }
        OutputStream responseBodyStream = exchange.getResponseBody();
        try {
            exchange.getResponseHeaders().add("Content-Type", "application/json");
            exchange.sendResponseHeaders(response.getCode(), response.getBody().getBytes().length);
            responseBodyStream.write(response.getBody().getBytes());
        } catch (IOException e) {
            System.out.println("!!! Could not send response");
            e.printStackTrace();
        } finally {
            try {
                responseBodyStream.close();
            } catch (IOException e) {
                System.out.println("!! Could not close socket");
            }
        }
    }
}

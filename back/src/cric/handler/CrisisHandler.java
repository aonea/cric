package cric.handler;

import com.amazonaws.util.IOUtils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import cric.handler.repositories.CrisisRepo;
import cric.handler.util.RequestType;
import cric.handler.util.Response;
import cric.handler.util.verification.Verification;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLOutput;

public class CrisisHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) {
        Response response;
        try {
            if (!Verification.verifyIdentity(Verification.getCookie(exchange.getRequestHeaders()))) {
                response = new Response("forbidden", 403);
            } else {
                switch (RequestType.getRequestType(exchange.getRequestMethod(), exchange.getRequestURI().getPath())) {
                    case GET_CRISES_FROM_ARIA: {
                        response = new CrisisRepo().getCrisis();
                        break;
                    }
                    case POST_CRISIS: {
                        if (!Verification.adminRequiredVerification(Verification.getCookie(exchange.getRequestHeaders()))) {
                            response = new Response("", 403);
                        } else {
                            String crisisJson = IOUtils.toString(exchange.getRequestBody());
                            response = new CrisisRepo().postCrisis(crisisJson);

                        }
                        break;
                    }
                    case UPDATE_CRISIS: {
                        if (!Verification.adminRequiredVerification(Verification.getCookie(exchange.getRequestHeaders()))) {
                            response = new Response("", 403);
                        } else {
                            String crisisJson = IOUtils.toString(exchange.getRequestBody());
                            String idCrisis = exchange.getRequestURI().getPath().split("/")[2];
                            response = new CrisisRepo().updateCrisis(crisisJson, idCrisis);
                        }
                        break;
                    }
                    default: {
                        response = new Response("", 400);
                        break;
                    }
                }
            }
        } catch (IOException e) {
            response = new Response("", 500);
        }
        sendResponse(exchange, response);
    }

    private void sendResponse(HttpExchange exchange, Response response) {
        System.out.println("someone reached crisis " + exchange.getRequestURI().getPath());
        exchange.getResponseHeaders().set("application/json", "text/html");
        OutputStream responseBodyStream = exchange.getResponseBody();
        try {
            exchange.sendResponseHeaders(response.getCode(), response.getBody().getBytes().length);
            responseBodyStream.write(response.getBody().getBytes());
        } catch (IOException e) {
            System.out.println("!!! Could not send response");
        } finally {
            try {
                responseBodyStream.close();
            } catch (IOException e) {
                System.out.println("!!! Could not close output stream");
            }
        }
    }
}

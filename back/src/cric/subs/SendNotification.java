package cric.subs;

import com.google.gson.Gson;
import cric.subs.pushservice.*;
import org.apache.http.HttpResponse;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

public class SendNotification {
    private final PublicKey publicKey = cric.subs.pushservice.Utils.loadPublicKey("BKvn4XSfj7kKnY3a2tdT3fUCRmglrFX3ba8DtEwc5-WqLkNokuIoiiOardgnxQNlmVRA7GcprEm-a4RBh7Z_fj8");
    private final PrivateKey privateKey = cric.subs.pushservice.Utils.loadPrivateKey("Un0AgQVYBJAxDqXTZ-dmEVrRwWgsi31RJ_VTNJZyuN8");

    public SendNotification() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {
    }

    public void run(String subscriptionJson, String payload) throws Exception {
        PushService pushService = new PushService()
                .setPublicKey(publicKey)
                .setPrivateKey(privateKey)
                .setSubject("mailto:admin@domain.com");

        Subscription subscription = getSubscription(subscriptionJson);

        Notification notification = new Notification(subscription, payload);

        HttpResponse response = pushService.send(notification);

        System.out.println(response);
    }

    private Subscription getSubscription(String jsonSub) {
        Gson gson = new Gson();

        return gson.fromJson(jsonSub, Subscription.class);
    }
}
